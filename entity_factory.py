from components.ai import HostileEnemy
from components.fighter import Fighter
from entity import Actor


player = Actor(
    char="@",
    color=(255, 255, 255),
    name="Player",
    ai_cls=HostileEnemy,
    fighter=Fighter(hp=30, defense=2, power=5),
    )

orc = Actor(
    char="o",
    color=(0, 27, 0),
    name="Orc",
    ai_cls=HostileEnemy,
    fighter=Fighter(hp=10, defense=0, power=3),
    )

troll = Actor(
    char="T",
    color=(206, 15, 28),
    name="Troll",
    ai_cls=HostileEnemy,
    fighter=Fighter(hp=16, defense=1, power=4),
    )
